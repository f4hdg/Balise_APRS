# Balise APRS

!!!!!!!! Projet en cours de upload !!!!!!!!

Création d'une balise APRS a associer a un emetteur VHF.
Permet l'envoie automatique d'une trame Packet tout les XXmin contenant les informations APRS
- type : ballon, voiture, maison...
- position : GPS
- Température
- Tension d'alimentation

## Materiel :
- Ublox NEO-6 (GPS)
- Arduino Mini Pro
- UART USB

## Logiciel : 
- Tracduino : https://github.com/trackuino/trackuino

## Modification faites
- changement du type de capteur
- modification du "type" d'emetteur passage de ballon a pieton
- delai d'envoie (prevu dans le config.h)